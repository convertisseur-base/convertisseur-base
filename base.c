/*
 Copyright (C) 2009 JAKSE Raphaël (rjakse@gmail.com)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

// If you are on Windows, uncomment this.
//#define WINDOWS

/*** DEBUT DU PROGRAMME **/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

// Permet de lire une chaîne de caractère rentree par l'utilisateur de façon securisee.
	int lire(char *chaine, int longueur);
// Permet de lire un entier long de la même façon.
	long lireLong();

int main(int argc, char* argv[])
{

	char *source = NULL,      // representera le nombre a convertir, sous forme de chaîne de caractère

	     src[255] = {0},           /* Utilisee sur le nombre n'a pas ete passe en argument.
	                             source pointera dans ce cas sur src.
	                             Si par contre le nombre a ete passe en argument,
	                             source pointera vers cet argument.
	                          */
	     chiffre=0;             // chiffre du nombre a convertir actuellemnt traîte

	unsigned char baseSrc=0,    // base du nombre a convertir (char parce que ça prend moins de place en memoire que int)
	              baseDest=0;   // base vers laquel on convertit

	unsigned long resultat=0, // nombre convertit en base 10
	     i=0,                 // variable des boucles
	     taille_source=0;     // nombre de chiffres du nombre a convertir

	long j=0;                  // variable utilisee dans une boucle lors de la traduction base 10 => base finale.
	                          // Valeur finale : -1, ce qui explique le type signe ("signed")

	char silencieux = 0;       // silencieux : ne pas afficher les etapes
	                          // 0 = faux, 1 = vrai


/** ÉTAPE 1 : ON GÈRE LES ARGUMENTS PASSÉS AU PROGRAMME */
	if(argc > 1)
	{ // S'il y a des arguments, on les gère.

		int numero_argument = 0;
		i = 1; // le premier argument (argv[0]) est le nom du programme, on s'en fiche

		while(i<argc)
		{ // on parcourt les arguments
			if(strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "-help") == 0 || strcmp(argv[i], "--help") == 0)
			{
			/* On affiche l'aide et on quite*/

				printf(" Usage : \n  base ([base_nombre_source] ([nombre_source] ([base_destination]))) (-q) \n\n     - base_nombre_source est la base du nombre a convertir\n     - nombre_source est le nombre a convertir\n     - base_destination est la base dans laquelle convertir le nombre\n\n  -q , --quiet : n'affiche que le resultat, pas les etapes.\n\n Si un ou des arguments ne sont pas fournis, ils vous seront demandes.\n Dans ce programme se cache une dedicace.\n\n(c)Raphael JAKSE 2009; ce programme est sous license GNU/GPL. Lisez le fichier LICENSE pour lire cette license.\n");
				return EXIT_SUCCESS;
			}

			else if(strcmp(argv[i], "-q") == 0 || strcmp(argv[i], "-quiet") == 0 || strcmp(argv[i], "--quiet") == 0)
			{
			/* on retourne le resultat quand on a termine plutôt que l'afficher */
				silencieux = 1;
			}

			E

			else
			{
				switch (numero_argument)
				{ // l'argument represente quoi ?
				case 0 : // le nombre a convertir
					source = argv[i]; // source pointe directement vers l'argument;

				/* On compte les chiffres du nombre a convertir */
					while(source[taille_source]!='\0')
					{
						++taille_source;
					}

					break;
				case 1 : // base du nombre a convertir
					baseSrc = strtol(argv[i], NULL, 10);
					break;
				case 2 : // base vers laquel convertir le nombre
					baseDest = strtol(argv[i], NULL, 10);
					break;
				default : // argument qui sert a rien.
					if(numero_argument == 3)
					{ // premier argument qui sert a rien
						printf("L'argument %lu ne sert a rien. \n", i);
					}
					else
					{ // Il y en a plus d'un qui sert a rien
						printf("L'argument %lu non plus.\n", i);
					}
				}
				++numero_argument;
			}
			++i;
		}

	}

	if(taille_source == 0)
	{ // on a pas passe le nombre a convertir en argument.

		printf("\nEntrez un nombre a convertir : ");

		if(lire(src, 255) == 0) // on lit et verifie que tout va bien.
		{
			printf("\n Une erreur s'est produite. Le programme se termine.");
	#ifdef WINDOWS
	system("pause");
	#endif
			return EXIT_FAILURE;
		}
		source = src;
		/** On compte le nombre de chiffres */
		while(source[taille_source]!='\0')
		{
			++taille_source;
		}
	}

	if(baseSrc == 0)
	{ // on a pas passe la base du nombre a convertir en argument.
		printf("\nEntrez la base dans laquelle est ce nombre.\nExemples : 10 (decimale), 16 (hexadecimale), 2 (binaire) : ");
		if((baseSrc = lireLong()) == 0) // on lit et verifie que tout va bien.
		{
			printf("\n Une erreur s'est produite.\nVous n\'avez peut-être pas entre un nombre representant une base.\nArrêt du programme.");
			return EXIT_FAILURE;
		}
	}

	if(baseDest == 0)
	{ // on a pas passe la base vers laquelle convertir le nombre en argument.
		printf("\nEntrez la base dans laquelle vous voulez le resultat : ");
		if((baseDest = lireLong()) == 0) // on lit et verifie que tout va bien.
		{
			printf("\n Une erreur s'est produite.\nVous n\'avez peut-être pas entre un nombre representant une base.\nArrêt du programme.");
	#ifdef WINDOWS
	system("pause");
	#endif
			return EXIT_FAILURE;
		}
	}

	if(!silencieux)
	{
	//on recapitule :
		printf("\nNombre a convertir : %s (%u)\nBase de destination : %u\n", source, baseSrc, baseDest);
	// on commence a afficher le resultat :
		printf("\n%s (%d) -> ",source,baseSrc); // "nombre (base) = "
	}

/** on parcourt tous les chiffres et on les "ajoute" au resultat en base 10 (conversion base x => base 10)*/

	i = 0;
	while(i != taille_source)
	{
		source[i]=toupper(source[i]); //on travaille en majuscule, parce que les minuscules, c'est mal. http://les_minuscules.saimal.fr/

	/** On convertit caractère chiffre => nombre en base 10 */

		if(source[i] < 58 && source[i] > 47) // chiffre entre 0 et 9
		{
			chiffre=source[i] - 48;
		}
		else if(source[i] > 64 && source[i] < 91) //chiffre entre A et Z
		{
			chiffre=source[i] - 55;
		}
		else
		{
			printf("... !!!\nErreur: le chiffre %c est inconnu.\n",source[i]);
	#ifdef WINDOWS
	system("pause");
	#endif
			return EXIT_SUCCESS;
		}

	/** Chiffre faisant partie d'une base plus grande que la base actuelle */
		if(chiffre>baseSrc-1)
		{
			printf("...\nErreur: le chiffre %c ne fait pas partie de la base %d.\n",source[i], baseSrc);
	#ifdef WINDOWS
	system("pause");
	#endif
			return EXIT_SUCCESS;
		}

		/*Chiffre suivant*/
		++i;

		/* Ajout au resultat*/
		resultat+=chiffre * pow(baseSrc, taille_source-i);

		/* On affiche les etapes*/
		if(!silencieux)
		{
			printf("%d * %u ^ %lu %s",chiffre, baseSrc, taille_source-i, (i==taille_source ? "\n":" + "));
		}
	}

	if(!silencieux)
	{
		/* Affichage du resultat*/
		printf("\n	-> %lu	(10)\n\n",resultat);
	}

	/** Si la base de destination n'est pas 10, conversion base 10 => base x */
	if(baseDest == 10)
	{
		if(silencieux)
		{ // on retourne le resulat plutôt que l'afficher si demande
			return resultat;
		}
	}
	else
	{
		int chiffres[255]; // Tableau de chiffres. Pas plus de 255 chiffres, j'ai decide :[
		i=0;

		do
		{
			chiffres[i]=resultat%baseDest;

			if(!silencieux)
			{
				/* On affiche les etapes*/
				printf("%s %d*%u^%lu",(i==0?" -> ":" + "),chiffres[i],baseDest,i);
			}
			++i;
		}	while(resultat/=baseDest); //tant que quotient non nul (qu'il reste des chiffres a trouver)


		j = i - 1; //nombre total de chiffres du nombre final

		if(!silencieux)
		{
		/* Attention, le resultat arrive !*/
			printf("\n\n	-> ");
		}

	/** On se fait tout les chiffres (conversion nombre/chiffre (et affichage))*/
		while(j != -1) // tant qu'il reste des chiffres
		{
		/** On convertit nombre en base 10 => caractère chiffre **/

			if(chiffres[j] > -1 && chiffres[j] < 10)
			{
				chiffre = chiffres[j] + 48;
			}
			else
			{
				chiffre = chiffres[j] + 55;
			}

			/* On affiche le chiffre*/
			printf("%c",chiffre);

			--j;
		}

		if(!silencieux)
		{
			/* On affiche la base dans laquel est le nombre qu'on vient de pondre*/
			printf("	(%d)",baseDest);
		}
		printf("\n");
	}
	#ifdef WINDOWS
	system("pause");
	#endif
	return EXIT_SUCCESS;
}

/** http://www.siteduzero.com/tutoriel-3-14185-la-saisie-de-texte-securisee.html */

	void viderBuffer()
	{
	    int c = 0;
	    while (c != '\n' && c != EOF)
	    {
		c = getchar();
	    }
	}

	int lire(char *chaine, int longueur)
	{
	    char *positionEntree = NULL;

	    if (fgets(chaine, longueur, stdin) != NULL)
	    {
		positionEntree = strchr(chaine, '\n');
		if (positionEntree != NULL)
		{
		    *positionEntree = '\0';
		}
		else
		{
		    viderBuffer();
		}
		return 1;
	    }
	    else
	    {
		viderBuffer();
		return EXIT_SUCCESS;
	    }
	}
	long lireLong()
	{
	    char nombreTexte[100] = {0}; // 100 cases devraient suffire

	    if (lire(nombreTexte, 100))
	    {
		// Si lecture du texte ok, convertir le nombre en long et le silencieux
		return strtol(nombreTexte, NULL, 10);
	    }
	    else
	    {
		// Si problème de lecture, renvoyer 0
		return EXIT_SUCCESS;
	    }
	}

/** */
